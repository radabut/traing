import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  constructor(
    private http:HttpClient
  ) { }

  add_member(data){
    return this.http.post('http://localhost:3000/post' , data)
  }

  get_member(){
    return this.http.get('http://localhost:3000/get')
  }

  delete_member(id){
    return this.http.delete('http://localhost:3000/delete/'+ id)
  }

  edit_member(data){
    return this.http.put('http://localhost:3000/put/'+ data.id , data)
  }


}
