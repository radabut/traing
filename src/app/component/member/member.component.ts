import { Component, OnInit } from '@angular/core';
import { MemberService } from 'src/app/service/member.service';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {

  name:any
  email:any
  address:any

  allMember:any

  edit_name:any
  edit_email:any
  edit_address:any

  constructor(
    private service : MemberService
  ) { }

  ngOnInit(): void {

    this.get_member();
  }

  add_member(){
    const addMember = {
      name:this.name,
      email:this.email,
      address:this.address
    }
    this.service.add_member(addMember).subscribe(req =>{
      console.log(req)
      this.ngOnInit();
    }, err =>{
      console.log(err)
    })
  }

  get_member(){
    this.service.get_member().subscribe(req => {
      this.allMember = req
      console.log(req)
    })
  }

  delete_member(id){
    this.service.delete_member(id).subscribe(req => {
      console.log(req)
      this.ngOnInit();
    })
  }

  edit_member(id){

    const edit = {
      id:id,
      name:this.edit_name,
      // email:this.edit_email,
      // adress:this.edit_address
    }

    this.service.edit_member(edit).subscribe(req => {
      console.log(req)
      this.ngOnInit();
    })
  }

}
