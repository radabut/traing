import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { MemberComponent } from './component/member/member.component';


const routers: Routes = [
    { path: '' , component: MemberComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routers, { useHash: true })],
    exports: [RouterModule ]
})

export class AppRoutingModule {}